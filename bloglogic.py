from flask import request, session
import sqlite3, hashlib, time, uuid

class Database(object):
    def __init__(self,dbname):
        self.dbname=dbname
    def connect(self):
        self.con = sqlite3.connect(self.dbname)
        self.sql = self.con.cursor()
    def close(self):
        self.con.close()

def sha(p): return hashlib.sha256(p).hexdigest()
def newsalt(): return uuid.uuid4().hex        
def logout(): session['account']=False

def login(db):
    '''check credentials, log user in'''
    user = request.form['user']
    passw = request.form['passw']
    db.connect()
    result = db.sql.execute("SELECT * FROM users WHERE username=? LIMIT 1", (user,)).fetchone()
    db.close()
    if result:
        salt = result[2]
        realhash = result[1]
        attemptedhash = sha(passw+salt)
        if realhash == attemptedhash:
            session['account']=user
            return True
        else: return False
    else: return False

def loggedin():
    '''check if user is currently logged in (session)'''
    try: return session['account']
    except: return False
    
def createpost(db):
    '''add new blog post to db'''
    title = request.form['posttitle']
    body = request.form['postbody']
    if title and body:
        db.connect()
        db.sql.execute("INSERT INTO posts VALUES (?,?,?)", (title, body, time.time()))
        db.con.commit()
        db.close()
        
def deletepost(db,postid):
    '''delete post by id'''
    db.connect()
    result = db.sql.execute('DELETE FROM posts WHERE rowid = ?',(postid,)).rowcount
    db.con.commit()
    db.close()
    return result
    
def blogposts(db):
    '''fetch & return list of all blog posts'''
    db.connect()
    result = db.sql.execute('SELECT title,body,time,rowid FROM posts ORDER BY time DESC').fetchall()
    db.close()
    return result
