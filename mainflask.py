from flask import *
import bloginit, bloglogic

app = Flask(__name__)
db = bloglogic.Database('blog.db')
bloginit.configure(db, 'johndoe', 'password123')
app.secret_key = 'EX4MPLEK3Y'

@app.route('/css/<filename>')
def css(filename):
    return send_from_directory('css',filename)

@app.route('/login',methods=['POST'])
def login():
    if bloglogic.login(db): return redirect('/')
    else: return 'login failed.'

@app.route('/logout')
def logout():
    bloglogic.logout()
    return redirect('/')

@app.route('/addpost',methods=['POST'])
def addpost():
    if bloglogic.loggedin(): 
        bloglogic.createpost(db)
        return redirect('/')
    else: return 'You left a field blank, or are no longer logged in.'

@app.route('/delete/<postid>')
def delpost(postid):
    if bloglogic.loggedin():
        bloglogic.deletepost(db, postid)
        return redirect('/')
    else: return "You can't delete a post unless you log in."
    
@app.route('/')
def home():
    loggedin = bloglogic.loggedin()
    posts = bloglogic.blogposts(db)
    return render_template('main.tpl', tplvars={'loggedin':loggedin, 'posts':posts})

app.run(host='localhost', port=80, debug=True)
